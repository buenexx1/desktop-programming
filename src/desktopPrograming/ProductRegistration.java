package desktopPrograming;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ProductRegistration extends JFrame implements ActionListener {

    private static JTextField colorText;
    private static JTextField amountText;
    private static JTextField nameText;
    private static JLabel color;
    private static JLabel name;
    private static JLabel amount;
    private static JButton button;
    private static JButton buttonClean;
    private static JButton buttonExit;

    public void initialize(){

        JPanel panel = new JPanel();

        this.setSize(300, 400);
        this.setTitle("Product Registration");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(panel);

        name = new JLabel("Name: ");
        name.setBounds(30, 60, 120, 30);
        panel.add(name);
        nameText = new JTextField(20);
        nameText.setBounds(25, 60, 200, 30);
        panel.add(nameText);

        color = new JLabel("Color: ");
        color.setBounds(30, 60, 120, 30);
        panel.add(color);
        colorText = new JTextField(20);
        colorText.setBounds(25, 60, 200, 30);
        panel.add(colorText);

        amount = new JLabel("Amount: ");
        amount.setBounds(30, 60, 120, 30);
        panel.add(amount);
        amountText = new JTextField(20);
        amountText.setBounds(25, 60, 200, 30);
        panel.add(amountText);

        buttonExit = new JButton("exit");
        buttonExit.setBounds(10, 80, 80, 25);
        buttonExit.addActionListener(actionEvent -> {
            this.setVisible(false);
            this.dispose();
            System.exit(0);
        });
        panel.add(buttonExit);

        button = new JButton("save");
        button.setBounds(10, 80, 80, 25);
        button.addActionListener(new ProductRegistration());
        panel.add(button);

        buttonClean = new JButton("Cancel");
        buttonClean.setBounds(10, 80, 80, 25);
        buttonClean.addActionListener(actionEvent -> {
            nameText.setText("");
            colorText.setText("");
            amountText.setText("");
        });
        panel.add(buttonClean);

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object[] attributes = {
                "name: " + nameText.getText(),
                "color: " + colorText.getText(),
                "amount: " + amountText.getText()
        };

        JOptionPane.showMessageDialog(null, attributes);
    }
}
